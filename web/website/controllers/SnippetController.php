<?php

class SnippetController extends Website\Controller\Frontend
{
    public function init()
    {
        parent::init();

        if ($this->editmode) {
            $this->enableLayout();
        }
    }

    public function defaultAction()
    {
    }
}
