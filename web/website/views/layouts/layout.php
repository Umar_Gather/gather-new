<!DOCTYPE html>
<html lang="<?= $this->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#00325b">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?php

    // output the collected meta-data
    if (!$this->document) {
        // use "home" document as default if no document is present
        $this->document = \Pimcore\Model\Document::getById(1);
    }

    if ($this->document->getTitle()) {
        // use the manually set title if available
        $this->headTitle()->set($this->document->getTitle());
    }

    if ($this->document->getDescription()) {
        // use the manually set description if available
        $this->headMeta()->appendName('description', $this->document->getDescription());
    }

    echo $this->headTitle();
    echo $this->headMeta();

    ?>
    <link href="/website/static/bundles/styles.css" rel="stylesheet">
</head>

<body>
    <?php $this->template('/templates/header.php'); ?>
        <?= $this->layout()->content; ?>
    <?php $this->template('/templates/footer.php'); ?>

    <!-- scripts -->
    <?= $this->polyfill(); ?>
    <?php if (PIMCORE_DEBUG) : ?>
        <script src="http://localhost:3000/middleware.js"></script>
    <?php endif; ?>
    <script src="/website/static/bundles/root.js"></script>
</body>
</html>
