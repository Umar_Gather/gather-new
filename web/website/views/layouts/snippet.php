<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Snippet</title>
    <link href="/website/static/bundles/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
    <?= $this->layout()->content; ?>

    <?php if (PIMCORE_DEBUG) : ?>
        <script src="http://localhost:3000/middleware.js"></script>
    <?php endif; ?>
    <script src="/website/static/bundles/root.js"></script>
</body>
</html>
