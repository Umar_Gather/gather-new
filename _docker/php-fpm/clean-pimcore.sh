#!/usr/bin/env bash

# resets Pimcore environment when anything drastic happens
echo "Cleaning Pimcore Environment..."

echo "Clear Pimcore Classmap"
rm -f "/application/web/website/config/autoload-classmap.php"
echo "Done!"

echo "Rebuilding Pimcore class definitions ..."
php "/application/web/pimcore/cli/console.php" --environment=development deployment:classes-rebuild -c -v
echo "Done!"

echo "Rebuilding Pimcore classmap ..."
php "/application/web/pimcore/cli/console.php" --environment=development classmap-generator -v
echo "Done!"

echo "Clearing Pimcore Cache ..."
php "/application/web/pimcore/cli/console.php" --environment=development cache:clear -v
echo "Done!"
