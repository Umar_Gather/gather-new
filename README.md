## Installation

Replace all instances of CHANGEME with the project name

## Development

See [Using Docker](./docs/01_DevelopmentGuide.md)

### Directory Structure

* flats - static HTML files as per the design
* web - the pimcore root folder
* release - the output folder for the Makefile
