/*

    pimcore-webpack-middleware
    
    provides live reloading for php, js, etc

    thom barlow

    Add the following snippet to your layout to enable;

    <?php if (PIMCORE_DEBUG) : ?>
        <script src="http://localhost:3000/middleware.js"></script>
    <?php endif; ?>

*/

let WebSocket = require('ws');
let fs = require('fs');
let webpack = require('webpack');
let webpackConfig = require('../webpack.config.js')({
    dev: true,
    static: '/../web/website/static/bundles',
});

// inject script

if (~process.argv.indexOf('--inject')) {
    console.log('Injecting script...');

    let injectScript = __dirname + '/inject.js';

    if (!webpackConfig.entry.root) {
        console.error('Middleware requires an entrypoint named \'root\'');
        process.exit();
    }

    if (Array.isArray(webpackConfig.entry.root)) {
        webpackConfig.entry.root.push(injectScript);
    }
    else {
        webpackConfig.entry.root = [webpackConfig.entry.root, injectScript];
    }
}

// create compiler

let compiler = webpack(webpackConfig);

// start server

let server = require('http')
    .createServer((req, res) => {
        if (req.url == '/middleware.js') {
            fs.readFile(__dirname + '/middleware.js', 'utf8', (err, data) => {
                res.end(data);
            });
        }
        else {
            res.end('you\'re lost, try another port');
        }
    })
    .listen(3000);

// attach socket

let wss = new WebSocket.Server({server});

// run middleware

compiler.watch({

    aggregateTimeout: 300,
    poll: true

}, (err, stats) => {

    if (err) {
        console.error(err)
    }
    else {
        console.log(stats.toString({colors:true, chunks:false}));
        console.log('Sending reload signal...');
        reload();
    }

});

// also check templates, controllers, etc for changes

require('chokidar')
    .watch([
        '../web/website/views',
        '../web/website/controllers',
        '../web/website/models',
        '../web/website/lib',
        '../web/website/static',
    ], {ignored: /[\/\\]\./}).on('change', reload);

// send reload signal to active clients

function reload() {
    wss.clients.forEach((client) => {
        if (client.readyState == WebSocket.OPEN) {
            client.send('RELOAD');
        }
    }); 
}
