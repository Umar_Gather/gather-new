import { preload } from '#util/preload';
import { tween, lerp, ease } from '#util/animate';

let lists = [];

[...document.querySelectorAll('[data-custom-collapse]')]
    .forEach((node) => {
        let obj = {
            node,
            open: true,
            content: node.nextElementSibling,
        };
        node.addEventListener('click', (e) => {
            toggle(obj);
        });

        lists.push(obj);
    });

function toggle(obj, instant = false) {
    if (window.innerWidth > 576) return;
    const { node, open, content } = obj;
    if (instant && open) {
        content.style.height = 0;
    } else if (instant) {
        content.style.height = null;
    } else if (open) {
        const { height } = content.getBoundingClientRect();
        tween((t) => {
            const i = lerp(height, 0, ease(t));
            content.style.height = i + 'px';
        }, 500);
        content.style.opacity = '0';
    } else {
        content.style.height = null;
        const { height } = content.getBoundingClientRect();
        content.style.height = 0;
        tween((t) => {
            const i = lerp(0, height, ease(t));
            content.style.height = i + 'px';
            if (t >= 1) {
                content.style.height = null;
            }
        });
        content.style.opacity = '1';
    }
    obj.open = !obj.open;
}

lists.forEach((obj) => {
    toggle(obj, true);
});
