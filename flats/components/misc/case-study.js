let currentTab = 0;
let prevBtn = document.getElementById('prevBtn');
let nextBtn = document.getElementById('nextBtn');
let wrapper = document.querySelector('.studies-wrapper');

function nextPrev(n) {
    let studies = document.querySelectorAll('.studies');
    studies[currentTab].style.display = 'none';
    currentTab += n;
    showTab(currentTab);
}

function showTab(currentTab) {
    let studies = document.querySelectorAll('.studies');
    studies[currentTab].style.display = 'block';
    if (currentTab == 0){
        prevBtn.style.display = 'none';
    } else {
        prevBtn.style.display = 'block';
    }
    if (currentTab == (studies.length - 1)){
        nextBtn.style.display = 'none';
    } else {
        nextBtn.style.display = 'block';
    }
}

if (wrapper) {
    prevBtn.addEventListener('click',  () => nextPrev(-1));
    nextBtn.addEventListener('click', () => nextPrev(1));
    document.addEventListener('DOMContentLoaded', () => showTab(currentTab));
}


