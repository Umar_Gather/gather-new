let isAdvancedUpload = function(){
    let div = document.createElement( 'div' );
    return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
};

let forms = document.querySelectorAll( '.box' );
forms.forEach((form) => {
    let input = form.querySelector( 'input[type="file"]' ),
        label = form.querySelector( 'label' ),
        droppedFiles = false,
        showFiles = (files) => {
            label.textContent = files[0].name;
        };
    input.addEventListener( 'change', (e) => {
        showFiles( e.target.files );
    });

    if(isAdvancedUpload){
        form.classList.add( 'has-advanced-upload' );
        [ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach((event) =>{
            form.addEventListener( event, (e) => {
                e.preventDefault();
                e.stopPropagation();
            });
        });
        [ 'dragover', 'dragenter' ].forEach(() => {
            form.addEventListener(event, () => {
                form.classList.add( 'is-dragover' );
            });
        });
        [ 'dragleave', 'dragend', 'drop' ].forEach( (event) => {
            form.addEventListener( event, (event) => {
                form.classList.remove( 'is-dragover' );
            });
        });
        form.addEventListener( 'drop', (e) => {
            droppedFiles = e.dataTransfer.files;
            showFiles( droppedFiles );
        });
    }

    input.addEventListener( 'focus', () => { input.classList.add( 'has-focus' ); });
    input.addEventListener( 'blur', () => { input.classList.remove( 'has-focus' ); });

});