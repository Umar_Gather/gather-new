const scrollIndicator = document.querySelector('.mobile-scroll-indicator');
function getDocHeight() {
    let D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}

function amountscrolled(){
    let winheight= window.innerHeight || (document.documentElement || document.body).clientHeight;
    let docheight = getDocHeight();
    let scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
    let trackLength = docheight - winheight;
    let pctScrolled = Math.floor(scrollTop/trackLength * 100); // gets percentage scrolled (ie: 80 or NaN if tracklength == 0)
    
    if (pctScrolled > 1) {
        scrollIndicator.classList.add('hide');
    } else {
        scrollIndicator.classList.remove('hide');
    }
}
 
if (scrollIndicator) {
    window.addEventListener('scroll', function(){
        amountscrolled();
    }, false);
}
