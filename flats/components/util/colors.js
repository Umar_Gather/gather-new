export function colorToArray(str) {
    // #000
    if (str.length == 4) {
        return str.slice(1).split``.map((c) => parseInt(c+c, 16));
    }
    // #000000
    else if (str.length == 7) {
        return str.slice(1).match(/../g).map((c) => parseInt(c, 16));
    }
    // rgb / rgba
    else {
        return str.match(/\d+/g).splice(0,3);
    }
}

function arrayToColor(arr) {
    return '#' + [...arr].map((d) => {d = d.toString(16); return d.length>1?d:'0'+d;}).join``;
}
