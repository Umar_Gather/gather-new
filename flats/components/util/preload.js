import { pimcoreReady } from '#util/ready';

const imgLoader = (img) => new Promise((resolve, reject) => {
    if (img.complete) {
        resolve();
    }
    else {
        img.addEventListener('load', resolve);
        img.addEventListener('error', resolve);
    }
});

export function preload(images, callback) {
    let imgPromises = Array.from(images).map((img) => imgLoader(img));

    Promise
        .all(imgPromises)
        .then((d) => {
            pimcoreReady(() => {
                callback(null, true);
            });
        })
        .catch((e) => {
            callback(e);
            console.error(`Error: this should never happen ${e}`);
        });
}
