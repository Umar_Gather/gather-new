/* eslint-disable */

export function pimcoreReady(callback) {

    if (typeof pimcore != 'object' || !Ext) {
        callback(false);
    }
    else {
        Ext.onReady(() => {
            ~function loop() {
                setTimeout(() => {
                    if (editablesReady) {
                        callback(true);
                    }
                    else {
                        loop();
                    }
                });
            } ();
        });
    }

}
