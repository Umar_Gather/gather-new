export function responder(callback) {

    document.addEventListener('DOMContentLoaded', () => {
        callback(window.innerWidth, window.innerHeight);
    });

    window.addEventListener('resize', () => {
        callback(window.innerWidth, window.innerHeight);
    });

}
