Gather Digital Frontend Buildtools
=====

# Notes

`yarn` can be substituted with `npm` in all these commands unless otherwise specified, but you are encouraged to use yarn to ensure accurate dependency versions.

bash is also highly recommended, but these commands will (mostly) work in powershell.

# Minimum Requirements

* Node >=6.x
* NPM >=3.x

# Commands

`yarn start` starts development server in live reloading mode

`yarn run middleware` starts middleware server for live development in pimcore

`yarn run format-styles` fixes .scss files to conform to .stylelintrc

`yarn run flats` creates a static version of the frontend assets to `artefact.tar.gz`

`yarn run deploy` creates a production build to `web/website/static/bundles/`

# Directory Tree

```
├── components/     - main directory for script files
│   ├── base/       - common components used across projects
│   ├── store/      - contains state classes
│   ├── util/       - general helper functions
│   └── root.js     - entrypoint
│
│── styles/         - main directory for styles
│   ├── bootstrap/  - bootstrap modules config
│   ├── util/       - general helper mixins / variables
│   └── root.scss   - entrypoint
│
├── middleware/     - tools for developing with pimcore
└── templates/      - static HTML files and other assets
```
